from django.contrib import admin
from .models import *


# Register your models here.
class VideoAdmin(admin.ModelAdmin):
    list_display = ['id', 'owner_id', 'status', 'created_at', 'updated_at']
    list_filter = ['created_at', 'updated_at','like', 'watch_amount','guess_amount','word_id' ]
    date_hierarchy = 'created_at'


class StatusAdmin(admin.ModelAdmin):
    list_display = ['id', 'description', 'created_at', 'updated_at']
    list_filter = ['created_at', 'updated_at']


class LevelAdmin(admin.ModelAdmin):
    list_display = ['id', 'description', 'created_at', 'updated']
    list_filter = ['created_at', 'updated']


class ProfileAdmin(admin.ModelAdmin):
    list_filter = ['score', 'games_win', 'games_amount']


admin.site.register(Word)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Status, StatusAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Level, LevelAdmin)
admin.site.register(Rule)
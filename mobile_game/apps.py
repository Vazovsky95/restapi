from django.apps import AppConfig


class MobileGameConfig(AppConfig):
    name = 'mobile_game'
    verbose_name = 'Мобильная игра'

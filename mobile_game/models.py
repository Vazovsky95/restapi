from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver


class Level(models.Model):
    class Meta:
        verbose_name = 'Уровень'
        verbose_name_plural = 'Уровни'

    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, default='', verbose_name="Название")
    created_at = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.description


class Rule(models.Model):
    class Meta:
        verbose_name = 'Правило'
        verbose_name_plural = 'Правила'

    text = models.CharField(max_length=1024)

    def __str__(self):
        return self.text


class Word(models.Model):
    class Meta:
        verbose_name = 'Слово'
        verbose_name_plural = 'Слова'

    name = models.CharField(max_length=255)
    level_id = models.ForeignKey(Level, on_delete=models.DO_NOTHING, related_name='Уровень')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Status(models.Model):
    class Meta:
        verbose_name = 'Статус'
        verbose_name_plural = 'Статус'

    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, default='', verbose_name="Название")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.description


class Video(models.Model):
    class Meta:
        verbose_name = 'Видео'
        verbose_name_plural = 'Видео'

    url = models.FileField(upload_to='video/%Y/%m/%d/', blank=True, null=False, verbose_name='Видео')
    owner_id = models.ForeignKey(User, on_delete=models.DO_NOTHING, verbose_name='Автор', blank=True)
    word_id = models.ForeignKey(Word, on_delete=models.CASCADE, verbose_name='Загаданное слово', blank=True)
    thumbnails = models.ImageField(upload_to='thumbnail/%Y/%m/%d/', blank=True, verbose_name="Изображение видео")
    like = models.IntegerField(verbose_name='Количество лайков', blank=True, default=0)
    watch_amount = models.IntegerField(verbose_name='Количество просмотров', blank=True, default=0)
    guess_amount = models.IntegerField(verbose_name='Количество правильных ответов', blank=True, default=0)
    status = models.ForeignKey(Status, on_delete=models.DO_NOTHING, verbose_name='Статус', blank=True)
    added_scores = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Добавлено')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')


@receiver(pre_save, sender=Video)
def update_stock(sender, instance, **kwargs):
    if instance.added_scores is False and instance.status.name == '2':
        instance.added_scores = True
        instance.save()
        instance.owner_id.profile.score += 10
        instance.owner_id.profile.save()


class Profile(models.Model):
    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профиль'

    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='Логин')
    photo = models.ImageField(upload_to='profile_photo/%Y/%m/%d/', blank=True, verbose_name="Фото профиля")
    rating = models.IntegerField(blank=True, default=0, verbose_name="Рейтинг")
    score = models.IntegerField(blank=True, default=0, verbose_name="Очки")
    games_amount = models.IntegerField(blank=True, default=0, verbose_name="Количество игр")
    games_win = models.IntegerField(blank=True, default=0, verbose_name="Количество выигранных игр")
    token_device = models.CharField(max_length=255, blank=True, null=True, verbose_name="Токен пользователя")
    created_at = models.DateTimeField(auto_now_add=True,verbose_name="Создано")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Обновлено")
    
    def __str__(self):
        return self.user.username

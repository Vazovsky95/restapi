from django.core.exceptions import SuspiciousOperation
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import serializers, status
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import NotFound

from .models import *


class LevelSerializers(serializers.ModelSerializer):
    class Meta:
        model = Level
        fields = ['name', 'created_at', 'updated']

    def create(self, validated_data):
        return Level.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.save()
        return instance


class RuleSerializers(serializers.ModelSerializer):
    class Meta:
        model = Rule
        fields = ['text']

    def create(self, validated_data):
        return Rule.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.text = validated_data.get('text', instance.text)
        instance.save()
        return instance


class WordSerializers(serializers.ModelSerializer):
    class Meta:
        model = Word
        exclude = ()

    def create(self, validated_data):
        return Word.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.level_id = validated_data('level_id', instance.level_id)
        instance.save()
        return instance


class StatusSerializers(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = ['name', 'created_at', 'updated_at']

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.save()
        return instance


class VideoSerializers(serializers.ModelSerializer):
    word = serializers.CharField(required=False, allow_blank=True, max_length=100)
    level = serializers.IntegerField(allow_null=True, write_only=True)

    class Meta:
        model = Video
        fields = ('id', 'url', 'owner_id', 'thumbnails', 'word', 'level', 'status', 'like', 'watch_amount', 'guess_amount', 'created_at')

    def create(self, validated_data):
        new_word = validated_data.get('word')
        level = validated_data.get('level')
        owner = validated_data.get('owner_id')
        if new_word is None or level is None or owner is None:
            raise NotFound('not found')
        level_id = get_object_or_404(Level, name=level)
        if level_id is None:
            raise NotFound('not found')
        obj, created = Word.objects.get_or_create(name=new_word, level_id=level_id)
        if obj is None:
            raise NotFound('not found')
        else:
            return Video.objects.create(
                url=validated_data.get('url'),
                thumbnails=validated_data.get('thumbnails'),
                word_id=obj,
                owner_id=owner,
                status=get_object_or_404(Status, name=1)
            )

    def update(self, instance, validated_data):
        if self.data.get('like'):
            instance.like = validated_data.get('like', instance.like)
        if self.data.get('watch_amount'):
            instance.watch_amount = validated_data.get('watch_amount', instance.watch_amount)
        if self.data.get('quess_amount'):
            instance.quess_amount = validated_data.get('quess_amount', instance.quess_amount)
        instance.url = validated_data.get('url', instance.url)
        instance.thumbnails = validated_data.get('thumbnails', instance.thumbnails)
        instance.save()
        return instance


class ProfileSerializers(serializers.ModelSerializer):
    class Meta:
        model = Profile
        exclude = ()

    def create(self, validated_data):
        return Profile.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.user = validated_data.get('user', instance.user)
        instance.rating = validated_data.get('rating', instance.rating)
        instance.score = validated_data.get('score', instance.score)
        instance.token_device = validated_data.get('token_device', instance.token_device)
        instance.save()
        return instance


class ProfileSerializer(serializers.ModelSerializer):
    rating = serializers.SerializerMethodField()
    video_amount = serializers.SerializerMethodField()
    photo = serializers.ImageField(allow_empty_file=True, allow_null=True)

    class Meta:
        model = Profile
        fields = ('rating', 'score', 'games_amount', 'games_win', 'video_amount', 'photo')
        read_only_fields = ('rating', 'score', 'games_amount', 'games_win', 'video_amount')

    def get_rating(self, obj):
        i = 1
        for p in Profile.objects.order_by('-score'):
            if p == obj:
                return i
            i += 1

    def get_video_amount(self, obj):
        return Video.objects.filter(owner_id=obj.user.id, status__id=2).__len__()


class UserSerializers(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    username = serializers.CharField(max_length=255)
    first_name = serializers.CharField(max_length=255)
    last_name = serializers.CharField(max_length=255)
    email = serializers.CharField(max_length=255)
    password = serializers.CharField(required=True,
                                     style={'input_type': 'password', 'placeholder': 'Пароль'},
                                     write_only=True)
    profile = ProfileSerializer(read_only=True)
    photo = serializers.ImageField(write_only=True, allow_empty_file=True, allow_null=True, required=False)

    def create(self, validated_data):
        if User.objects.filter(username=validated_data['username']).exists():
            api_exception = serializers.ValidationError('username is busy')
            api_exception.status_code = status.HTTP_409_CONFLICT
            raise api_exception
        else:
            print(validated_data)
            new_user = User(
                email=validated_data['email'],
                username=validated_data['username'],
                first_name=validated_data['first_name'],
                last_name=validated_data['last_name']
            )
            new_user.set_password(validated_data['password'])
            new_user.save()
            Token.objects.create(user=new_user)
            profile = Profile.objects.create(user=new_user)
            try:
                if validated_data['photo'] is not None:
                    profile.photo = validated_data['photo']
                    profile.save()
            except Exception as e:
                print(e)
            return new_user

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.profile.photo = validated_data.get('photo', instance.profile.photo)
        instance.profile.save()
        instance.save()
        return instance


class GameStartSerializer(serializers.ModelSerializer):
    word_id = serializers.StringRelatedField()
    word = serializers.CharField(source='word_id')

    class Meta:
        model = Video
        fields = ('id', 'url', 'word_id', 'word', 'thumbnails')


class UserVideosSerializer(serializers.ModelSerializer):
    word_id = serializers.StringRelatedField()
    word = serializers.CharField(source='word_id')
    created_at = serializers.DateTimeField(format="%d.%m.%Y %H:%M", required=False, read_only=True)
    level = serializers.SerializerMethodField()

    class Meta:
        model = Video
        fields = ('id', 'url', 'word_id', 'word', 'thumbnails', 'like', 'watch_amount', 'guess_amount', 'created_at', 'status', 'level', 'added_scores')

    def get_level(self, obj):
        return Level.objects.get(id=obj.word_id.level_id_id).name


class RatingSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'profile')

from django.urls import path, include
from rest_framework import routers
from mobile_game import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'level', views.LevelViewSet)
router.register(r'rule', views.RuleViewSet)
router.register(r'word', views.WordViewSet)
router.register(r'status', views.StatusViewSet)
router.register(r'profile', views.ProfileViewSet)
router.register(r'video', views.VideoUpload)

urlpatterns = [
    path('', include(router.urls)),
    path('game/start', views.GameStartView.as_view(), name='upload_video'),
    path('user/<int:pk>/stat', views.UserStatViewSet.as_view(), name='user_stat'),
    path('user/<int:pk>/videos', views.UserVideosView.as_view(), name='user_videos'),
    path('video/<int:pk>/stat', views.VideoStatViewSet.as_view(), name='video_stat'),
    path('rating/', views.RatingViewSet.as_view(), name='rating')
]

from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework import viewsets, generics
from rest_framework.exceptions import NotFound
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet, mixins

from .serializers import *


# Create your views here.


class LoginView(APIView):
    permission_classes = ()

    def post(self, request):
        username = request.data.get("username")
        password = request.data.get("password")
        user = authenticate(username=username, password=password)
        if user:
            return Response({"token": user.auth_token.key,
                             "id": user.id})
        else:
            return Response({"error": "Wrong Credentials"}, status=status.HTTP_400_BAD_REQUEST)


class LevelViewSet(GenericViewSet, mixins.RetrieveModelMixin,
                   mixins.CreateModelMixin, mixins.UpdateModelMixin,
                   mixins.DestroyModelMixin):
    serializer_class = LevelSerializers
    queryset = Level.objects.all()

    def get(self, request):
        word = Level.objects.all()
        serializer = LevelSerializers(word, many=True)
        return Response({"Level": serializer.data})

    def put(self, request, pk):
        saved_level = get_object_or_404(Level.objects.all(), pk=pk)
        data = request.data.get('level')
        serializers = LevelSerializers(instance=saved_level, data=data, partial=True)
        level_saved = serializers.save()
        return Response({"success": "Level '{}' updated successfully".format(level_saved.name)})

    def delete(self, request, pk):
        delete_level = get_object_or_404(Level.objects.all(), pk=pk)
        delete_level.delete()
        return Response({"success": "Level '{}' delete successfully".format(delete_level.name)})


class RuleViewSet(GenericViewSet, mixins.RetrieveModelMixin,
                  mixins.CreateModelMixin, mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin):
    serializer_class = RuleSerializers
    queryset = Rule.objects.all()

    def get(self, request):
        word = Rule.objects.all()
        serializer = RuleSerializers(word, many=True)
        return Response({"Rule": serializer.data})

    def put(self, request, pk):
        saved_rule = get_object_or_404(Rule.objects.all(), pk=pk)
        data = request.data.get('rule')
        serializers = RuleSerializers(instance=saved_rule, data=data, partial=True)
        rule_saved = serializers.save()
        return Response({"success": "Rule '{}' updated successfully".format(rule_saved.text)})

    def delete(self, request, pk):
        delete_rule = get_object_or_404(Rule.objects.all(), pk=pk)
        delete_rule.delete()
        return Response({"success": "Rule '{}' delete successfully".format(delete_rule.text)})


class WordViewSet(GenericViewSet, mixins.RetrieveModelMixin,
                  mixins.CreateModelMixin, mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin):
    serializer_class = WordSerializers
    queryset = Word.objects.all()

    def get(self, request):
        word = Word.objects.all()
        serializer = WordSerializers(word, many=True)
        return Response({"Word": serializer.data})

    def put(self, request, pk):
        saved_word = get_object_or_404(Word.objects.all(), pk=pk)
        data = request.data.get('word')
        serializers = WordSerializers(instance=saved_word, data=data, partial=True)
        word_saved = serializers.save()
        return Response({"success": "Words '{}' updated successfully".format(word_saved.name)})

    def delete(self, request, pk):
        delete_words = get_object_or_404(Word.objects.all(), pk=pk)
        delete_words.delete()
        return Response({"success": "Words '{}' successfully deleted ".format(delete_words.name)})


class StatusViewSet(GenericViewSet, mixins.RetrieveModelMixin,
                    mixins.CreateModelMixin, mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin):
    serializer_class = StatusSerializers
    queryset = Status.objects.all()

    def get(self, request):
        status = Status.objects.all()
        serializer = StatusSerializers(status, many=True)
        return Response({"Status": serializer.data})

    def put(self, request, pk):
        saved_status = get_object_or_404(Status.objects.all(), pk=pk)
        data = request.data.get('status')
        serializers = StatusSerializers(instance=saved_status, data=data, partial=True)
        status_save = serializers.save()
        return Response({"Success": "Status '{}' updated successfully".format(status_save.name)})

    def delete(self, request, pk):
        delete_status = get_object_or_404(Status.objects.all(), pk=pk)
        delete_status.delete()
        return Response({"Success": "Status '{}' successfully deleted".format(delete_status.name)})


class ProfileViewSet(GenericViewSet, mixins.RetrieveModelMixin,
                     mixins.CreateModelMixin, mixins.UpdateModelMixin,
                     mixins.DestroyModelMixin):
    serializer_class = ProfileSerializers
    queryset = Profile.objects.all()

    def get(self, request):
        profile = Profile.objects.all()
        serializer = ProfileSerializers(profile, many=True)
        return Response({"Profiles": serializer.data})

    def put(self, request, pk):
        saved_profile = get_object_or_404(Profile.objects.all(), pk=pk)
        data = request.data.get('profile')
        serializers = ProfileSerializers(instance=saved_profile, data=data, partial=True)
        profile_saved = serializers.save()
        return Response({"Success": "Profile '{}' updated successfuly".format(profile_saved.user)})

    def delete(self, request, pk):
        delete_profile = get_object_or_404(Profile.objects.all(), pk=pk)
        delete_profile.delete()
        return Response({"Success": "Profile '{}' successfuly deleted".format(delete_profile.user)})


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializers

    def put(self, request, pk):
        saved_user = get_object_or_404(User.objects.all(), pk=pk)
        data = request.data.get('user')
        serializers = UserSerializers(instance=saved_user, data=data, partial=True)
        user_saved = serializers.save()
        return Response({"Success": "User '{}' updated successfully".format(user_saved.username)})

    def delete(self, request, pk):
        delete_user = get_object_or_404(User.objects.all(), pk=pk)
        delete_user.delete()
        return Response({"Success : User '{}' delete successfully".format(delete_user.username)})


class VideoUpload(GenericViewSet, mixins.RetrieveModelMixin,
                  mixins.CreateModelMixin, mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin):
    parser_classes = (FormParser, MultiPartParser)
    queryset = Video.objects.all()
    serializer_class = VideoSerializers

    def get(self, request):
        articles = Video.objects.all()
        serializer = VideoSerializers(articles, many=True)
        return Response({"Video": serializer.data})

    def post(self, request, *args, **kwargs):
        file_serializers = VideoSerializers(data=request.data)
        file_serializers.save()
        return Response(file_serializers.data, status=status.HTTP_201_CREATED)

    def put(self, request, pk):
        if self.request.data.get('type') == 'like':
            saved_likes = get_object_or_404(Video.objects.all(), pk=pk)
            data = request.data.get('like')
            serializers = VideoSerializers(instance=saved_likes, data=data, partial=True)
            like_saved = serializers.save()
            return Response(like_saved.data, status=status.HTTP_201_CREATED)
        if self.request.data.get('type') == 'watch_amount':
            saved_watch = get_object_or_404(Video.objects.all(), pk=pk)
            data = request.data.get('watch_amount')
            serializers = VideoSerializers(instance=saved_watch, data=data, partial=True)
            watch_saved = serializers.save()
            return Response(watch_saved.data, status=status.HTTP_201_CREATED)
        if self.request.data.get('type') == 'quess_amount':
            saved_quess = get_object_or_404(Video.objects.all(), pk=pk)
            data = request.data.get('quess_amount')
            serializers = VideoSerializers(instance=saved_quess, data=data, partial=True)
            quess_saved = serializers.save()
            return Response(quess_saved.data, status=status.HTTP_201_CREATED)
        else:
            saved_videos = get_object_or_404(Video.objects.all(), pk=pk)
            data = request.data.get('video')
            serializers = VideoSerializers(instance=saved_videos, data=data, partial=True)
            video_saved = serializers.save()
            return Response(video_saved.data)

    def delete(self, request, pk):
        delete_video = get_object_or_404(Video.objects.all(), pk=pk)
        delete_video.delete()
        return Response({"Success : User '{}' delete successfully".format(delete_video.username)})


class GameStartView(generics.ListAPIView):
    serializer_class = GameStartSerializer

    def list(self, request):
        if self.request.GET.get('level') is None:
            level = 1
        else:
            level = self.request.GET.get('level')

        video = Video.objects.filter(word_id__level_id__name=level, status__id=2).order_by("?").first()

        if video is not None:
            serializer = GameStartSerializer(video, context={'request': request})
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class UserStatViewSet(generics.UpdateAPIView):
    def update(self, request, pk, format=None, *args, **kwargs):
        if self.request.GET.get('type') is None:
            raise NotFound('not found')
        else:
            type = self.request.GET.get('type')

        user = get_object_or_404(User.objects.all(), pk=pk)
        if type == 'play':
            user.profile.games_amount += 1
            response = Response({'amount': user.profile.games_amount})
        elif type == 'win':
            user.profile.games_win += 1
            response = Response({'amount': user.profile.games_win})
        else:
            scores = request.data.get('scores')
            if scores is None:
                raise NotFound('not found')
            user.profile.score = scores
            response = Response({'amount': user.profile.score})
        user.profile.save()
        return response


class VideoStatViewSet(generics.UpdateAPIView):
    def update(self, request, pk, format=None, *args, **kwargs):
        if self.request.GET.get('type') is None:
            raise NotFound('not found')
        else:
            type = self.request.GET.get('type')

        video = get_object_or_404(Video.objects.all(), pk=pk)
        if type == 'like':
            video.like += 1
            video.owner_id.profile.score += 2
            video.owner_id.profile.save()
            response = Response({'amount': video.like})
        elif type == 'guess':
            video.guess_amount += 1
            response = Response({'amount': video.guess_amount})
        elif type == 'watch':
            video.watch_amount += 1
            response = Response({'amount': video.watch_amount})
        else:
            response = Response(status=status.HTTP_404_NOT_FOUND)
        video.save()
        return response


class UserVideosView(generics.ListAPIView):
    serializer_class = GameStartSerializer

    def list(self, request, pk):
        video = Video.objects.filter(owner_id=pk)

        if video is not None:
            serializer = UserVideosSerializer(video, context={'request': request}, many=True)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class RatingViewSet(generics.ListAPIView):
    serializer_class = RatingSerializer

    def list(self, request):
        users = User.objects.order_by('-profile__score')

        if users is not None:
            serializer = RatingSerializer(users, context={'request': request}, many=True)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)
